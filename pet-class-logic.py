import random
Hunger_TH = 5
Boredom_TH = 5

class Pet:
    def __init__(self,name,hunger,boredom,sounds):
        self.name = name
        self.hunger = random.randint(0, 10)
        self.boredom = random.randint(0,10)
        self.sounds = sounds
        status = ''

    def sta_tus(self):
        if self.hunger >= Hunger_TH and self.boredom >= Boredom_TH:
            status = 'Happy'
            return status
        elif self.hunger <= Hunger_TH :
            status = 'Hungry'
            return status
        elif self.boredom <= Boredom_TH:
            status = 'Bored'
            return status

    def __str__(self):
        if self.hunger >= Hunger_TH and self.boredom >= Boredom_TH:
            status = 'Happy'
            return 'Status of {0} :Happy'.format(self.name)
        elif self.hunger <= Hunger_TH :
            status = 'Hungry'
            return 'Status of {0} :Hungry'.format(self.name) 
        elif self.boredom <= Boredom_TH:
            status = 'Bored'
            return 'Status of {0} :Bored'.format(self.name)

    def clock_tick(self):
        self.boredom+=1
        self.hunger+=1

    def reduce_hunger(self):
        if self.hunger != 0:
            self.hunger-=1
            print('Feed Done')
        else:
            print('Pet had Enough Food')

    
    def reduce_boredom(self):
        if self.boredom != 0:
            self.boredom -=1


    def feed(self):
        self.reduce_hunger()

    
    def hi(self):
        r_word = random.choice(self.sounds)
        self.reduce_boredom()
        return r_word
        
    def teach(self,p_naam):
        word = input('Enter the word to be taught to {0}'.format(p_naam))
        self.sounds.append(word)
        print(self.sounds)
        self.reduce_boredom()
    



user_name = input("Enter your Name: ")
available_pets =[]
#options available for the user


print('Welcome {} '.format(user_name))
ch = 'y'
object_list=[]
while ch == 'y':
    print('Options Available: \n 1. Adopt a New Pet \n 2. Feed your Pet \n 3. Interact with your Pet')

    if available_pets == []:
        print('No Pets Available!!! You need to Adopt one')
        
    op = int(input('Enter an Option: '))
#Pet Adoption
    if op == 1:
        pet_name = input("Enter your Pet's Name:")
        p1 = Pet(pet_name,0,0,[]) 
        available_pets.append(p1.name)
        object_list.append(p1)
        print(p1)
        
#Feed Pet
    if op == 2:
        naam = input('Pet to be Fed: ')
        if naam in available_pets:
            for p in object_list:
                p.clock_tick()
                s = p.sta_tus()
                if s == 'Hungry':
                    p.feed()
                   
            
#Interact with Pet
    if op == 3:
        naam_b = input('Pet to be Interacted With: ')
        if naam_b in available_pets:
            for m in object_list:
                m.clock_tick()
                st =  m.sta_tus()
                if st == 'Bored':
                    print('How do you like to interact with {0}? \n 1. Teach a New Word \n 2. Play  \n'.format(naam_b))
                    K = int(input("Enter a Valid Input: "))
                    if K == 1:
                        m.teach(naam_b)
                        print('Teach Done')
                    elif K == 2:
                        W = m.hi()
                        print('Interaction Done with {0}'.format(W))
#User Prompt to Continue Multiple Times              
    user_response = input('Do you want to continue: (y/Y)')
    ch = user_response.lower()

